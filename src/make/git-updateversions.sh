#!/bin/bash
#
# @author Zachary Wartell
#
# Goals is to make this into a pre-commit hook, but I don't have time now...
#
# - https://www.atlassian.com/git/tutorials/git-hooks
# - https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks
# OR
# - alternatively, replace this .sh script with a Node.js script
#
# Resources 
# - https://cd34.com/blog/programming/using-git-to-generate-an-automatic-version-number/ 
# - https://softwareengineering.stackexchange.com/questions/141973/how-do-you-achieve-a-numeric-versioning-scheme-with-git
# - https://stackoverflow.com/questions/460297/git-finding-the-sha1-of-an-individual-file-in-the-index



# update project version file "VERSION.txt"
dirRevisionCount=`git log --oneline . | wc -l`     # number of revision to current directory
branch=`git status . | grep -e "On branch" | sed -e "s/On branch \(\w*\)/\1/"`
if [[ $branch = "master" ]]; then
    branch=""
else
    branch=-$branch
fi
echo $branch
#exit

sed -i -e "s/\([0-9]\.\).*/\1"$dirRevisionCount"/" VERSION.txt

rm -f VERSION_NAME.txt
echo -n "Alpha-" > VERSION_NAME.txt
cat VERSION.txt >> VERSION_NAME.txt
sed -i -e "s/\([0-9]\.\).*/\1"$dirRevisionCount$branch"/" VERSION_NAME.txt

cat VERSION.txt
echo
cat VERSION_NAME.txt

version=`cat VERSION_NAME.txt`

# version.js not use in this project yet
# sed -i -e "s/\(\/*@product_version\*\/\).*;/\1'"`cat VERSION_NAME.txt`"';/" version.js

sed -i -e "s/\(\"version\": \"\)\([^\"]*\)\"/\1"`cat VERSION.txt`"\"/" manifest.json

sed -i -e "s/\(\"version_name\": \"\)\([^\"]*\)\"/\1"`cat VERSION_NAME.txt`"\"/" manifest.json
#exit
pushd ..
sed -i -e "s/\(##### Version: \) \(.*\)/\1 "`cat src/VERSION_NAME.txt`"/" README.md
egrep -e ".*Version:.*" README.md
#cat README.md
popd


sed -i -e "s/\(<!--version-->\)[^<]*/\1"`cat VERSION_NAME.txt`"/" *.html

#
# Note, I haven't add the tags to any .js files yet so the code below is not yet useful in this project
#

echo "$cleanversion.$dirRevisioncount"




# get all modified files (git status) in current directory ; note, quotes added for filenames with spaces
FILES=`git status --short . | grep "^ M" | sed -e "s/ M //;s/\"//g"`
#echo
#echo FILES
echo Modified: $FILES

# update file versions of files named on command-line
# for (( i = $#; i > 0; i--)); do
OIFS="$IFS"
IFS=$'\n'
for FILE_ in $FILES ; do
    #echo FILE: $FILE
    FILE=${FILE_//\"} #strip quotes
    if [[ -f $FILE ]]; then
        #FILE=$(eval echo $FILE)
        echo Updating version tags in "$FILE"

        # get file revision found
        fileRevisionCount=`git log --oneline "$FILE" | wc -l`    # number of revisions to file

        # increment by one since file is modified and about to be git commit'ed
        fileRevisionCount=$(( $fileRevisionCount + 1 ))

        # get project version
        projectVersion=`git describe --always --tags --long`   # not used right now... considering using SHA too.
        cleanVersion=${projectversion%%-*}

        # modify @version line in file
        if [[ ! $FILE =~ "git-updateversions.sh" ]]; then
            sed -i -E -e "s/@version[[:space:]]*[^[:space:]]*/@version "`cat VERSION.txt | sed "s/\([[:digit:]]*\).*/\1.x/"`"-"$fileRevisionCount"/" "$FILE"
        fi
    fi
done
IFS="$OIFS"

#echo "$projectVersion-$dirRevisionCount-$fileRevisionCount"
echo "$cleanversion.$dirRevisioncount"