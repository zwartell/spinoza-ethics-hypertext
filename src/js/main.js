/**
 * @author Zachary Wartell
 * @copyright Zachary Wartell.  All rights reserved. 2019.
 *
 * @version 1.x-33
 */

/**
 * @author Zachary Wartell
 */
class Chapter
{
    constructor()
    {
        let init = null;
        if (arguments.length !== 0 && typeof arguments[0] === 'object')
            init = arguments[0];

        /** {Number} number of this chapter */
        this.number = init !== null ? (init.number || 0) : 0;
        /** {string} name of this chapter */
        this.name = init !== null ? (init.name || null) : null;
        /** {string} name attribute of anchor of chapter */
        this.nameID = init !== null ? (init.nameID || null) : null;
        this.partName  = init !== null ? (init.partName || null) : null;
        /** {Object: HTML Element} HTML anchor element marking chapter */
        this.anchor = init !== null ? (init.anchor || null) : null;

        this.fullName = this.partName  + " " + this.name;
    }
}

/**
 * @author Zachary Wartell
 */
class DefinitionSection
{
    constructor()
    {
        let init = null;
        if (arguments.length !== 0 && typeof arguments[0] === 'object')
            init = arguments[0];

        this.header = init !== null ? (init.header || null) : null;
        this.name = init !== null ? (init.name || null) : null;
        this.chapter = init !== null ? (init.chapter || null) : null;
        this.definitions = [];
    }
}

/**
 * @author Zachary Wartell
 */
class PropositionSection
{
    constructor()
    {
        let init = null;
        if (arguments.length !== 0 && typeof arguments[0] === 'object')
            init = arguments[0];

        this.header = init !== null ? (init.header || null) : null;
        this.name = init !== null ? (init.name || null) : null;
        this.chapter = init !== null ? (init.chapter || null) : null;
        this.propositions = [];
    }
}

/**
 *
 */
class Term
{
    constructor()
    {
        let init = null;
        if (arguments.length !== 0 && typeof arguments[0] === 'object')
            init = arguments[0];

        this.term = init !== null ? (init.term || null) : null;
        this.definition = init !== null ? (init.definition  || null) : null;
    }
}

/**
 * @author Zachary Wartell
 */
class DebugConsole
{
   constructor()
   {
       let init = null;
       if (arguments.length !== 0 && typeof arguments[0] === 'object')
           init = arguments[0];

       this.level = init !== null ? (init.level || 0) : 0;
   }

    /**
     * @author Zachary Wartell
     * @param {Number} debugLevel
     * @param {...} args - passed to console.log
     */
   log()
   {
       var args = Array.from(arguments);
       let level=args.shift();
       if (this.level >= level)
            console.log(...args);
   }
}

var dbgConsole = new DebugConsole({level : 1});


/**
 * @author Zachary Wartell
 */
class AxiomSection
{
    constructor()
    {
        let init = null;
        if (arguments.length !== 0 && typeof arguments[0] === 'object')
            init = arguments[0];

        this.header = init !== null ? (init.header || null) : null;
        this.name = init !== null ? (init.name || null) : null;
        this.chapter = init !== null ? (init.chapter || null) : null;
        this.axioms = [];
    }
}
/**
 * @author Zachary Wartell
 */
class Axiom
{
    constructor()
    {
        let init = null;
        if (arguments.length !== 0 && typeof arguments[0] === 'object')
            init = arguments[0];

        this.number = init !== null ? (init.number || null) : null;
        this.paragraph = init !== null ? (init.paragraph || null) : null;
        this.axiomSection = init !== null ? (init.axiomSection  || null) : null;
    }
}

/**
 * @author Zachary Wartell
 */
class Definition
{
    constructor()
    {
        let init = null;
        if (arguments.length !== 0 && typeof arguments[0] === 'object')
            init = arguments[0];

        this.number = init !== null ? (init.number || null) : null;
        this.paragraph = init !== null ? (init.paragraph || null) : null;
        this.term = init !== null ? (init.term || null) : null;
        this.definitionSection = init !== null ? (init.definitionSection || null) : null;
    }
}

/**
 * @author Zachary Wartell
 */
class Proposition
{
    constructor()
    {
        let init = null;
        if (arguments.length !== 0 && typeof arguments[0] === 'object')
            init = arguments[0];

        this.number = init !== null ? (init.number || null) : null;
        this.paragraph = init !== null ? (init.paragraph || null) : null;
        this.term = init !== null ? (init.term || null) : null;
        this.propositionSection = init !== null ? (init.propositionSection || null) : null;
    }
}

/**
 *
 */
class Reference
{
    constructor()
    {

    }
}


const chapters=[];
const definitions=[];
const terms=[];
const propositions=[];
const axioms=[];
const propositionSections=[];
const axiomSections=[];
const definitionSections=[];
const references=[];
const INSANE_HACK = true;

/**
 *  InfoDialog encapsulates a HTML5 dialog that shows a string message
 */
class InfoDialog
{
    constructor()
    {
        this.dialog_=null;
        this.jqDialog_=null;
        this.input=null;
    }

    get dialog() { return this.dialog_; }

    /*
     * show this StudentDialog
     */
    show(message)
    {
        if (this.dialog_ === null)
        {
            this.dialog_ = document.createElement("div");
            this.dialog_.setAttribute("id", "infoDialog");

            this.dialog_.innerHTML =
                `                   
                <div>     
                    <p> <span id="message"><span> </p>
                </div>                    
                `;
            document.body.insertAdjacentElement('afterbegin',this.dialog_);

            this.jqDialog_=$( "#infoDialog" );
            this.jqDialog_.dialog({
                title: "Proof Element Info",
                position: { my: "right top", at: "right top", of: window },
                modal: false,
                draggable: true,
                autoOpen: false,
                width: 400,
                /* none needed, dialog auto-closes
                buttons: [
                    {
                        text: "Ok",
                        click: function() {
                            $( this ).dialog( "close" );
        }
                    },
                    {
                        text: "Cancel",
                        click: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                ]
                */
            });
            this.jqDialog_.dialog("open");
        }
        else {
            this.jqDialog_.dialog({position: { my: "right top", at: "right top", of: window}});
            this.jqDialog_.dialog("open");
        }

        let dialog_msg = document.querySelector("div#infoDialog span#message");
            dialog_msg.innerText = message;
        }

    /**
     *
     */
    close()
    {
        this.jqDialog_.dialog( "close");
    }
}
var infoDialog = new InfoDialog();

/**
 *  infoDialog.show(msg);
 */
function generic_onmouseenter(e) {
    //let msg = e.target.innerText;//e.target.innerText.replace("\n"," ");// + "<br>" + e.target.id;
    let str = "class: " + e.target.getAttribute("class") + "\n" + "text: " + e.target.innerText;
    dbgConsole.log(1,str);
    infoDialog.show(str);
}

/**
 *  infoDialog.close(msg);
 */
function generic_onmouseout(e) {
        infoDialog.close();
    }

/**
 * Assign generic mouse handles to all HTML Span element in NodeList spans
 * @param {NodeList} spans
 */
function assign_generic_mousehandlers(spans)
{
    for (let s of spans) {s.onmouseover = generic_onmouseenter; s.onmouseout = generic_onmouseout; }
}

/**
 *
 * @param {HTML Element} p
 * @param {regex} whole_regex
 * @param {regex} refs
 * @param {string} className
 * @param {boolean} matched_substring
 */
function markup_reference(p, whole_regex, refs, className,matched_substring)
{
    if (arguments.length === 4)
        matched_substring = false;
    p.innerHTML = p.innerHTML.replace(whole_regex, "<span class='"+className+(matched_substring ? "'>$&</span>" : "'>$1</span>"));

    dbgConsole.log(1, "--- ref's regex: " + refs.length);
    dbgConsole.log(2, refs);
}

/**
 *
 */

/**
 * identifyAndMarkupReference
 *
 * @param refs
 * @param col_regex1
 * @param text
 * @param p
 * @returns {*}
 */
function identifyAndMarkupReference(regex, text, p,className)
{
    let refs = regex.exec(text);
    if (refs !== null) {
        markup_reference(p, regex, refs, className, true);
        references.push (new Reference());
    }
}

/**
 * @author Zachary Wartell
 *
 * [In Progress] Traverse DOM and hyperlink all references to their definitions.
 * Status: starting to wrap some of them with <span></span>
 *
 * @param {Chapter} chapter
 * @param {Chapter} chapter_next
 */
function traverseAndHyperlinkReferences(chapter, chapter_next)
{
    /*
    * \todo make these regex's more maintainable ... partially done :)
    */
    const ref_regex1 =            /.*\([IVXL]+[.]\).*/gm;
    const ref_regex2 =            /\(([IVXL]+[.]?)\s*([ivxl]*[.]?)\)/gm;

    //e.g. (II. vii. Coroll.)
    const cor_regex1 =            /([IVXL]+\.)\s*([ivxl]+[.]?)[,]?\s*Coroll\./gm;

    //e.g. (II. vii. Coroll. i.)
    const cor_regex2 =            /([IVXL]+\.)\s*([ivxl]+[.]?)[,]?\s*Coroll\.\s*([ivxl]+[.]?)/gm;

    const cor_regex3 =            /(,|\()\s*Coroll\.\s*([ivxl]+[.]?)/gm;


    // (Ax. ) - other Chapter
    const ax_regex1 =        /([IVXL]+[.])\s*Ax(iom)?\.\s*([ivxl]*[.]?)/gm;///\(((by)?|(in)?)\s*([IVXL]+[.])\s*Ax\.\s*([ivxl]*[.]?)\)/gm;
    // (Ax. ) - current Chapter
    const ax_regex2 =        /Ax(iom)?\.\s*([ivxl]*[.]?)/gm;///\(((by)?|(in)?)\s*Ax\.\s*([ivxl]*[.]?)\)/gm;

    // (Def. ) - other Chapter
    const def_regex1 =       /([IVXL]+[.])?[,]?\s*Def[f]?\.\s*([ivxl]*[.]?)/gm;  // Note, some spelling errors occur, Deff.
    // (Def. ) - current Chapter
    //const def_regex2 =       /((by)?|(in)?)\s*Def\.\s*([ivxl]*[.]?)/gm;

    const lemma_regex1 =       /\(([IVXL]+[.])[,]?\s*Lemma\s*([ivxl]*[.]?)\)/gm;
    // (Def. ) - current Chapter
    const lemma_regex2  =       /\(((by)?|(in)?)\s*Lemma\s*([ivxl]*[.]?)\)/gm;

    const prop_regex1      = /Prop\.\s*([ivxl]+[.]?)/gm;///\(((by)?|(in)?)\s*Prop\.\s*([ivxl]*[.]?)\)/gm;
    const part_prop_regex = /\((Part\s*[ivxl]+[.]?)\s*,\s*(Prop\.\s*[ivxl]+[.]?)\)/gm;

    const post_regex1      = /\(((by)?|(in)?)\s*Post\.\s*([ivxl]*[.]?)\)/gm;

    const end = chapter_next === null ? null : chapter_next.anchor;
    let pc=0;
    for (let p = chapter.anchor; p != end; p = p.nextElementSibling) // 'propositionHeader'
    {
        if (p.tagName === "P")
        {
            const text = p.innerText;
            pc++;
            /**
             * \todo [PERFORMANCE] consider making the regex matching more efficient by combining into single regex (??? possible ???)
             * .. it could avoid searching 'text' over multiple passes ... perhaps
             */
            identifyAndMarkupReference(ref_regex1,      text, p,'defRef');
            identifyAndMarkupReference(ref_regex2,      text, p,'defRef');

            identifyAndMarkupReference(cor_regex1,      text, p,'corollaryRef');
            identifyAndMarkupReference(cor_regex2,      text, p,'corollaryRef');
            identifyAndMarkupReference(cor_regex3,      text, p,'corollaryRef');

            identifyAndMarkupReference(ax_regex1,       text, p,'axiomRef');
            identifyAndMarkupReference(ax_regex2,       text, p,'axiomRef');

            identifyAndMarkupReference(def_regex1,       text, p,'defRef');
            //identifyAndMarkupReference(def_regex2,       text, p,'defRef');

            identifyAndMarkupReference(lemma_regex1,       text, p,'lemmaRef');
            identifyAndMarkupReference(lemma_regex2,       text, p,'lemmaRef');


            identifyAndMarkupReference(part_prop_regex, text, p,'propRef');
            identifyAndMarkupReference(prop_regex1,      text, p,'propRef');

            identifyAndMarkupReference(post_regex1,      text, p,'postRef');

            assign_generic_mousehandlers(p.querySelectorAll("span.propRef , span.postRef , span.defRef , span.axiomRef , span.corollaryRef"));
        }
    }
    dbgConsole.log(1,"scanned " + pc + " paragraphs");
}

/**
 * @author Zachary Wartell
 *
 * Traverse DOM and extract all Proposition and PropositionSections between the two chapters 'chapter' and 'chapter_next'
 *
 * @param {Chapter} chapter
 * @param {Chapter} chapter_next
 */
function traverseAndExtractAxioms(chapter, chapter_next)
{
    /*
     *  find Axioms
     */

    // definition with PROP in title
    const ax_regex = /(Axiom|AXIOM)?\s*([IVXL]+[.]?)\s?/m;

    /*
    // definition without PROP in title but in PROP header'ed section
    const propNumOnly_regex = /^\s*([IVXL]+[.]?)\s/m;
    */

    const end = chapter_next === null ? null : chapter_next.anchor;
    for (let p = chapter.anchor; p != end; p = p.nextElementSibling) // 'axiomHeader'
    {
        //console.log("element: " + dh);
        if (p.tagName === "H4" || p.tagName === "H3")
        {
            let
                ahf = p.innerText.match(/AXIOMS\.?/), // axiomHeaderFound
                // some chapters don't have separate PROPOSITIONS heading
                phf = p.innerText.match(/PROPOSITIONS\.?/);    // propHeaderFound

            if (ahf || phf)
            {
                /*
                 * add proposition section
                 */
                let as = new AxiomSection(// 'propositionSection'
                    {
                        header: p,
                        chapter: chapter,
                        name: ahf ? "AXIOMS." : p.innerText.replace(".", "")
                    });
                axiomSections.push(as);

                /*
                * get propositions in section
                */
                for (let pi = p.nextElementSibling;  // paragraphInner
                     pi !== undefined && pi !== null && (pi.tagName === "P" || pi.tagName === "BR");
                     pi = pi.nextElementSibling)
                    if (pi.innerText !== null && pi.tagName == "P") {
                        let an;   // 'axiomNumber'

                        an = ax_regex.exec(pi.innerText);
                        if (an !== null)
                        {
                            if (ahf || (an.length === 3 && an[1] !== undefined))
                            {
                                let axiom = new Axiom(
                                    {
                                        number: an.length === 2 ? an[1].replace(".", "") : an[2].replace(".", ""),
                                        paragraph: pi,
                                        axiomSection: as
                                        //propositionSection: ps
                                    });
                                let classAttr = pi.getAttribute("class");
                                if (classAttr === null) {
                                    pi.setAttribute("class", "axiom");
                                }
                                pi.setAttribute("id", chapter.nameID + "-Axiom-" + axiom.number);

                                pi.onmouseover = function (e) {
                                    let str = "class: " + e.target.getAttribute("class") + "\n" + "id: " + e.target.id;
                                    infoDialog.show(str);
                                }
                                pi.onmouseout = function (e) {
                                    generic_onmouseout(e);
                                }

                                axioms.push(axiom);
                            }
                        }
                    }
            }
        }
    }
    //dbgConsole.log(1, "axioms section count:" + propositionSections.length);
    dbgConsole.log(1, "axioms count: " + axioms.length);
}

/**
 * @author Zachary Wartell
 *
 * Traverse DOM and extract all Proposition and PropositionSections between the two chapters 'chapter' and 'chapter_next'
 *
 * @param {Chapter} chapter
 * @param {Chapter} chapter_next
 */
function traverseAndExtractPropositions(chapter, chapter_next)
{
    /*
     *  find Propositions
     */

    // definition with PROP in title
    const prop_regex = /\s*PROP\.\s*([IVXL]+[.]?)\s/m;

    /*
    // definition without PROP in title but in PROP header'ed section
    const propNumOnly_regex = /^\s*([IVXL]+[.]?)\s/m;
    */

    const end = chapter_next === null ? null : chapter_next.anchor;
    for (let ph = chapter.anchor; ph != end; ph = ph.nextElementSibling) // 'propositionHeader'
    {
        //console.log("element: " + dh);
        if (ph.tagName === "H3" || ph.tagName === "H4")
        {
            //console.log("element: " + dh);
            let
                prhf = ph.innerText.match(/\s*PROPOSITIONS\.?\s*/), // propHeaderFound
                // some chapters don't have separate PROPOSITIONS heading
                pohf = ph.innerText.match(/\s*POSTULATES\.?\s*/)    // postulateHeaderFound

            if (prhf || pohf)
            {
                /*
                 * add proposition section
                 */
                let ps = new PropositionSection(// 'propositionSection'
                    {
                        header: ph,
                        chapter: chapter,
                        name: pohf ? "PROPOSITION" : ph.innerText.replace(".", "")
                    });
                propositionSections.push(ps);
                /*
                 * get propositions in section
                 */
                for (let p = ph.nextElementSibling; p !== undefined && p !== null && (p.tagName === "P" || p.tagName === "BR"); p = p.nextElementSibling)
                {
                    if (p.innerText !== null && p.tagName == "P") {
                        let pn;   // 'propositionNumber'

                        if (p.innerText.match(prop_regex))
                            pn = prop_regex.exec(p.innerText);
                        else
                            continue;

                        let prop = new Proposition(
                            {
                                number: pn[1].replace(".", ""),
                                paragraph: p,
                                propositionSection: ps
                            });
                        let classAttr = p.getAttribute("class");
                        if (classAttr === null) {
                            p.setAttribute("class", "proposition");
                        }
                        p.setAttribute("id", chapter.nameID + "-" +ps.name.replace(/ /g,"_") + "-" + prop.number);

                        p.onmouseover = function (e) {
                            let str = "class: " + e.target.getAttribute("class") + "\n" + "id: " + e.target.id;
                            infoDialog.show(str);
                        }
                        p.onmouseout = function (e) {
                            generic_onmouseout(e);
                        }

                        propositions.push(prop);
                        ps.propositions.push(prop);
                    }
                }
                if (ps.propositions.length === 0)
                    console.log("WARNING: empty propositionsSection: " + ps);
            }
        }
    }
    dbgConsole.log(1, "propositions section count:" + propositionSections.length);
    dbgConsole.log(1, "propositions count: " + propositions.length);
}


/**
 * @author Zachary Wartell
 *
 * Traverse DOM and extract all Definitions and DefinitionSections between the two chapters 'chapter' and 'chapter_next'
 *
 * @param {Chapter} chapter
 * @param {Chapter} chapter_next
 */
function traverseAndExtractDefinitions(chapter, chapter_next) {
    /*
     *  find Definitions
     */

    // definition with DEFINITION in title
    const def_regex = /\s*DEFINITION\s*([IVXL]+[.]?)[^,]/m;  // avoid catching "I, like this man,"

    // attempt to extract term in sentences like "Joy is ...."
    const defterm_regex1 = /^\s*(DEFINITION|\s*)\s*[IVXL]+[.]?\s*By\s*an\s*([\w-]*)/m;
    // attempt to extract term in sentences like "By virtue I mean ...."
    const defterm_regex2 = /^\s*(DEFINITION|\s*)\s*[IVXL]+[.]?\s*By\s*([\w\-]*)/m;
    // attempt to extract term in sentences like "Joy is ...." or "Honor[11] is ...." "Honor (synonym) is ...."
    const defterm_regex3 = /^\s*(DEFINITION|\s*)\s*[IVXL]+[.]?\s*([\w-]*)\s*(\[\d*\])?(\(\w*\))?\s*is/m;

    // definition without DEFINITION in title but in DEFINITION header'ed section
    const defNumOnly_regex = /^\s*([IVXL]+[.]?)[^,]/m;

    const end = chapter_next === null ? null : chapter_next.anchor;
    for (let dh = chapter.anchor; dh != end; dh = dh.nextElementSibling)
    {
        //console.log("element: " + dh);
        if (dh.tagName === "H3" || dh.tagName === "H4")
        {
            //console.log("element: " + dh);
            if (dh.innerText.match(/\s*DEFINITIONS\.?\s*/)) {
                /*
                 * add definition section
                 */
                let ds = new DefinitionSection(  // 'definitionSection'
                    {
                        header: dh,
                        chapter: chapter,
                        name: dh.innerText.replace(".", "")
                    });
                definitionSections.push(ds);
                /*
                 * get definitions in section
                 */
                for (let p = dh.nextElementSibling; p !== undefined && p !== null && (p.tagName === "P" || p.tagName === "BR"); p = p.nextElementSibling) {
                    if (p.innerText !== null && p.tagName == "P") {
                        let dn;
                        // extract definition number
                        if (p.innerText.match(defNumOnly_regex))
                            dn = defNumOnly_regex.exec(p.innerText);
                        else if (p.innerText.match(def_regex))
                            dn = def_regex.exec(p.innerText);
                        else
                            continue;

                        // try to get term being defined
                        let term = defterm_regex1.exec(p.innerText);
                        if (term === null) {
                            term = defterm_regex2.exec(p.innerText);
                            if (term === null)
                                term = defterm_regex3.exec(p.innerText);
                        }

                        // construct definition
                        let def = new Definition(
                            {
                                number: dn[1].replace(".", ""),
                                paragraph: p,
                                term: term !== null ? term [2] : null,
                                definitionSection: ds
                            });

                        // add to 'terms'
                        if (term !== null)
                        {
                            let termObj = new Term ({term: term[2], definition: def});
                            terms.push(termObj);
                        }

                        // insert class attribute and onmouseover behavior
                        let classAttr = p.getAttribute("class");
                        if (classAttr === null) {
                            p.setAttribute("class", "definition");
                        }
                        p.setAttribute("id", chapter.nameID + "-" +ds.name.replace(/ /g,"_") + "-" + def.number);
                        p.setAttribute("data-term", def.term);
                        if (term === null)
                            term = ["error","error","ERROR: not identified"];

                            p.innerHTML = p.innerHTML.replace(term[2], "<span class=''>" + term[2] + "</span>");
                            p.onmouseover = function (e) {
                                let span = e.target.querySelector("span");
                            let str;
                                if (span !== null) {
                                    span.setAttribute("class", "definitionTerm_Highlight");
                                    console.log(term);
                            } else

                                dbgConsole.log(1,e.target.id);

                            str = "class: " + e.target.getAttribute("class") + "\n" + "id: " + e.target.id;
                                infoDialog.show(str);
                            }
                            p.onmouseout = function (e) {
                                let span = e.target.querySelector("span");
                                if (span !== null)
                                    span.setAttribute("class", "");
                                generic_onmouseout(e);
                            }

                        // store definition
                        definitions.push(def);
                        ds.definitions.push(def);
                    }
                }
                if (ds.definitions.length === 0)
                    console.log("WARNING: empty definitionSection: " + ds);
            }
        }
    }
    dbgConsole.log(1, "definition section count:" + definitionSections.length);
    dbgConsole.log(1, "definition count: " + definitions.length);
    let tc = 0;//'termCount'
    for (let d of definitions) if (d.term !== null) tc++;
    dbgConsole.log(1, "definition term count: " + tc + "/" + definitions.length);
    dbgConsole.log(1, "chapter count: " + chapters.length);
}

/**
 * @author Zachary Wartell
 */
function onload() {
    /*
     *  redirect original hrefs to local file
     */
    for (let e of document.querySelectorAll("a[href*='https://www.gutenberg.org/files/3800/3800-h/3800-h.htm']"))
    {
        e.setAttribute("href","#" + e.getAttribute("href").split("#")[1]);
    }

    /*
     *  find Chapter heading
     */
    let c = 1;
    for (let e of document.querySelectorAll("a[name*=chap]")){// + h2,h4")) {
        dbgConsole.log(e.innerText);
        if (c === 1) {
            let pn_n = e.nextElementSibling.innerText.split("I.");
            chapters.push(new Chapter(
                {
                    number: c,
                    partName: pn_n [0] + " I.",
                    name: pn_n[1].trim(),
                    nameID: e.getAttribute("name"),
                    anchor: e
                }));
        } else {
            let t = e.nextElementSibling;
            //console.log(t);
            chapters.push(new Chapter(
                {
                    number: c,
                    partName: t.innerText,
                    name: t.nextElementSibling.innerText,
                    nameID: e.getAttribute("name"),
                    anchor: e
                }));
        }
        c++;
    }

    /*
     *  Extract Definitions
     */
    for (let c=0; c<chapters.length; c++)
    {
        traverseAndExtractDefinitions(chapters[c], c+1 === chapters.length ? null : chapters[c+1]);
        traverseAndExtractPropositions(chapters[c], c+1 === chapters.length ? null : chapters[c+1]);
        traverseAndExtractAxioms(chapters[c], c+1 === chapters.length ? null : chapters[c+1]);
        traverseAndHyperlinkReferences(chapters[c], c+1 === chapters.length ? null : chapters[c+1]);

    }


    dbgConsole.log(1,definitions);
    dbgConsole.log(1,terms);
    dbgConsole.log(1,propositions);
    dbgConsole.log(1,axioms);
    dbgConsole.log(1,definitionSections);
    dbgConsole.log(1,propositionSections);
    dbgConsole.log (1,chapters);

    dbgConsole.log(1,"  * " + chapters.length + " chapter headings identified");
    dbgConsole.log(1,"  * " + definitions.length + " definitions identified");
    dbgConsole.log(1,"  * " + propositions.length + " propositions identified");
    dbgConsole.log(1,"  * " + axioms.length + " axioms identified");
    dbgConsole.log(1,"  * " + definitionSections.length + " definition 'sections' identified");
    dbgConsole.log(1,"  * " + propositionSections.length + " propositions 'sections' identified");
    dbgConsole.log(1,"  * " + terms.length + " definitions terms identified (of " + definitions.length + " definitions)");
    dbgConsole.log(1,"  * " + propositionSections.length + " propositions 'sections' identified");
    dbgConsole.log(1,"  * " + references.length + " references  identified");
}