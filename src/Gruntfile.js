/**
@author Zachary Wartell

REFERENCES:
    - https://gruntjs.com/sample-gruntfile
    - https://www.npmjs.com/package/grunt-jsdoc
*/
module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        jsdoc : {
            dist : {
                src: ['smart_reply.js'],
                options: {
                    destination: 'docs/code',
                    //configure: 'make/jsdoc.conf'
                }
            }
        },
        shell: {
            options: {
                stderr: false
            },
            target: {
                command: 'bash ./make/git-updateversions.sh'  // this isn't working yet...
            }
        },
        watch: {
            doc: {
                files: ['<%= jsdoc.files %>'],
                tasks: ['jsdoc']
            },
            version: {
                files: ['*.js'],
                tasks: ['version']
            },
        }
    });

    grunt.loadNpmTasks('grunt-jsdoc');
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['jsdoc','shell']);
};