# Spinoza's Ethics - Hypertext

Editor: Zachary Wartell <br>
Code Author/Programmer: [Zachary Wartell](https://webpages.uncc.edu/zwartell/) <br>
Copyright: Prof. Zachary Wartell, University of North Carolina at Charlotte. 2019.
<br>
Caveats: This work does not represent UNCC and is a personal project of mine :) <br>
WWW: https://zwartell.gitlab.io/spinoza-ethics-hypertext/index.html

##### Version:  Alpha-1.58

## Introduction

### Goals

1. Create a version of Spinoza's The Ethics (Ethica Ordine Geometrico Demonstrata) where all internal citations 
   to textual elements such as:
    * definitions
    * [axioms](https://en.wikipedia.org/wiki/Axiom)
    * [propositions](https://en.wikipedia.org/wiki/Proposition)
    * Lemmas
    * Corollaries
    * etc. <br>
   are hyperlinked to their definitions.
2. Make an interactive visualization to represent structure of the proof (i.e. a [DAG](https://en.wikipedia.org/wiki/Directed_acyclic_graph) visualization).
3. Make a completely superfluous virtual reality version of the said visualization. 

### Why?

Because:

1. \<sarcasm> The world needs yet another hypertext version of Spinoza's Ethics \</sarcasm>
2. In 8th grade, I learned from a favorite sci-fi:satire author Douglas Adams that if you asked a "dumb" (=vague/poorly specified) question: "What is the answer to life, the universe and everything?", you will get a "dumb" (=non-useful) answer "42"! <br><br>
   I inferred that the remedy is to pursue sophisticated questions and to contemplate sophisticated answers.
3. Increasingly, I need a philosophy & programming side project to provide myself a rational path to some moments of serenity.
### Status: Work in Progress

Current Result: https://zwartell.gitlab.io/spinoza-ethics-hypertext/index.html

A number of the effects of the text processing code is not visible yet.  Additional debugging info is printed to the console.log()
(CTRL-SHFT-I on Chrome, etc.).

1. 2/10/2019
    * Data/Text Structure Extraction:
        * 5 chapter headings identified
        * 75 definitions identified
        * 259 propositions identified
        * 20 axioms identified
        * 5 definition 'sections' identified
        * 6 propositions 'sections' identified
        * 61 definitions terms identified (of 75 definitions)
        * 6 propositions 'sections' identified
        * 310 references  identified
            * identification in progress
        
        * Note: Overall I'm not sure what false positive and false negative rates are yet.
        
        * Chapters, Definitions, etc. are cross-linked to each other in 
          internal data structures.
        * Insertion of unique HTML Element id at each Definition, Axiom, Proposition  `<p></p>` element
                                 
    * User Visible Effects:                
        * Definitions             
            * 'onmouseover' 
                * Popup dialog displays some info. 
                * Highlighting the extracted Definition's defined term                
            * Borders around identified Definition `<p></p>` element
        * Proposition, Axiom 
            * 'onmouseover' - Popup dialog displays some info.       
            * Borders around identified item's `<p></p>` element
        * References to definitions, etc. 
            * Visual highlighting of references 
            * 'onmouseover' - a popup dialog displays some debug info. 
                * Note, identification of references is very 'in-progress' right now.                     

### TODO

#### Short-term

1. Continue regex updates for finding references
2. Generate hyperlinks for references
3. ... 


#### Long-term
1. Make some an navigatable index similar.  Possible resources
    * https://www.w3schools.com/howto/howto_js_sidenav.asp
2. All that other stuff           
     

## References
  
- The Ethics (Ethica Ordine Geometrico Demonstrata) by  Benedict de Spinoza
  Translated from the Latin by R. H. M. Elwes [WWW: https://www.gutenberg.org/files/3800/3800-h/3800-h.htm;
  EBook #3800].
